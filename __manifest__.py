# -*- coding: utf-8 -*-
# License AGPL-3.0 or later
{
    'name': "Snippet 1",
    'version': "10.0.1.0.0",
    'author': "Andrea Piovesana (OCA)",
    'license': "AGPL-3",
    'website': "http://www.openindustry.it",
    'category': "Tools",
    'depends': [
        'base',
    ],
    'application': True,
    'installable': True,
}
