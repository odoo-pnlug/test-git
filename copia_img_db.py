# -*- coding: utf-8 -*-
import openerplib
#Below script will read image from one db and write to second db.
# source database
SOURCE_USER = 'admin'
SOURCE_PASSWORD = 'a'
SOURCE_HOSTNAME = 'localhost'
SOURCE_DATABASE = 'odoo1'
SOURCE_PORT = 8888
connection = openerplib.get_connection(hostname=SOURCE_HOSTNAME, database=SOURCE_DATABASE, login=SOURCE_USER, password=SOURCE_PASSWORD, port=SOURCE_PORT)
# destination database
DEST_USER = 'admin'
DEST_PASSWORD = 'a'
DEST_HOSTNAME = 'localhost'
DEST_DATABASE = 'odoo2'
DEST_PORT = 8000
new_connection = openerplib.get_connection(hostname=DEST_HOSTNAME, database=DEST_DATABASE, login=DEST_USER, password=DEST_PASSWORD, port=DEST_PORT)
#product.product
source_product_model = connection.get_model("product.product") # source database product model
product_ids = source_product_model.search([]) # source product ids
destination_product_model = new_connection.get_model("product.product") # destination database product template model

for product_id in product_ids:
     old_product = source_product_model.read(product_id, ['image', 'ean13'])
     new_product_ids = destination_product_model.search([('ean13', '=', old_product['ean13'])])
     if new_product_ids:
         new_product = destination_product_model.read(new_product_ids[0], ['name', 'image'])
         if not new_product['image']:
             print("Product is updated with image...........",new_product['name'])
             destination_product_model.write(new_product_ids[0], {'image': old_product['image']})
print("Succesfully updated all products in database...........")


